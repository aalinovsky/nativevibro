//
//  com.alinouskiAppDelegate.h
//  nativevibro
//
//  Created by aalinovsky on 12/26/2018.
//  Copyright (c) 2018 aalinovsky. All rights reserved.
//

@import UIKit;

@interface alinouskiAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
