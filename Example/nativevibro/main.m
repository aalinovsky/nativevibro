//
//  main.m
//  nativevibro
//
//  Created by aalinovsky on 12/26/2018.
//  Copyright (c) 2018 aalinovsky. All rights reserved.
//

@import UIKit;
#import "com.alinouskiAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([com.alinouskiAppDelegate class]));
    }
}
