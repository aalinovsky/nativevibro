# nativevibro

[![CI Status](https://img.shields.io/travis/aalinovsky/nativevibro.svg?style=flat)](https://travis-ci.org/aalinovsky/nativevibro)
[![Version](https://img.shields.io/cocoapods/v/nativevibro.svg?style=flat)](https://cocoapods.org/pods/nativevibro)
[![License](https://img.shields.io/cocoapods/l/nativevibro.svg?style=flat)](https://cocoapods.org/pods/nativevibro)
[![Platform](https://img.shields.io/cocoapods/p/nativevibro.svg?style=flat)](https://cocoapods.org/pods/nativevibro)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

nativevibro is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'nativevibro'
```

## Author

aalinovsky, dvd2444@mail.ru

## License

nativevibro is available under the MIT license. See the LICENSE file for more info.
