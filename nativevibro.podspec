#
# Be sure to run `pod lib lint nativevibro.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'nativevibro'
  s.version          = '0.0.1'
  s.summary          = 'Simple script for making vibro'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.com/aalinovsky/nativevibro'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'aalinovsky' => 'dvd2444@mail.ru' }
  s.source           = { :git => 'https://bitbucket.com/aalinovsky/nativevibro.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

  s.source_files = 'nativevibro/Classes/**/*'
  
  # s.resource_bundles = {
  #   'nativevibro' => ['nativevibro/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
s.frameworks = 'AudioToolbox'
  # s.dependency 'AFNetworking', '~> 2.3'
end
