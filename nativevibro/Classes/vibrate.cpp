//
//  vibrate.cpp
//  vibrate
//
//  Created by BOSS on 19.12.2018.
//  Copyright © 2018 BOSS. All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>

enum VibrateType{
    Default = 1 << 0,
    Peek = 1 << 1,
    Pop = 1 << 2,
    Cancelled = 1 << 3,
    TryAgain = 1 << 4,
    Failed = 1 << 5
};

extern "C" void iosStartCustomVibrate(const int type)
{
    VibrateType vibr = (VibrateType)type;
    SystemSoundID val = SystemSoundID(1519);
    
    if(vibr & Peek) {
        val = SystemSoundID(1519);
    }
    
    if(vibr & Pop) {
        val = SystemSoundID(1520);
    }
    
    if(vibr & Cancelled) {
        val = SystemSoundID(1521);
    }
    
    if(vibr & TryAgain) {
        val = SystemSoundID(1102);
    }
    
    if(vibr & Failed) {
        val = SystemSoundID(1107);
    }
    
    AudioServicesPlaySystemSound(val);
}
